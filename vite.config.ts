/// <reference types="vite/client" />
import { defineConfig } from 'vite'
import eslint from 'vite-plugin-eslint'
import react from '@vitejs/plugin-react'
import viteTsconfigPaths from "vite-tsconfig-paths"

// https://vitejs.dev/config/
export default defineConfig({
  build: {
    outDir: "build",
    sourcemap: true,
    minify: "terser",
    rollupOptions: {
      output: {
        assetFileNames: asset => {
          switch (asset.name?.split(".").pop()) {
            case "css":
              return "css/" + `[name]` + ".min.css";
            case "png":
            case "jpg":
            case "ico":
            case "svg":
              return "images/" + `[name]` + `[extname]`;
            default:
              return "assets/" + `[name]` + `[extname]`;
          }
        },
        chunkFileNames: "chunks/" + `[name]` + `.min.js`,
        manualChunks: {
          react: ["react", "react-dom", "react/jsx-runtime"],
          router: ["react-router", "react-router-dom"],
          tanstack: ["@tanstack/react-query"],
          axios: ["axios"],
        },
      },
    },
  },
  plugins: [react(), 
    eslint({
    failOnError: true,
  }),
  viteTsconfigPaths(),
],
  server: {
    port: 3000,
    host: "127.0.0.1",
  },
})
