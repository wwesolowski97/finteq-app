import "./index.css";

import {
  QueryCache,
  QueryClient,
  QueryClientProvider,
  keepPreviousData,
} from "@tanstack/react-query";
import { StrictMode, Suspense, lazy } from "react";

import App from "App";
import { createRoot } from "react-dom/client";
import toast from "react-hot-toast";

const rootContainer = document.getElementById("root");

export const ReactQueryDevtools = lazy(() =>
  import("@tanstack/react-query-devtools").then(m => ({ default: m.ReactQueryDevtools })),
);

const QueryProvider = () => {
  const onError = (error: Error) => {
    console.error(error);
    toast.error(error.message);
  };

  const queryClient = new QueryClient({
    defaultOptions: {
      queries: {
        gcTime: 10000,
        placeholderData: keepPreviousData,
        refetchOnWindowFocus: false,
      },
    },
    queryCache: new QueryCache({
      onError,
    }),
  });

  return (
    <QueryClientProvider client={queryClient}>
      <App />
      <Suspense>
        <ReactQueryDevtools position="bottom" buttonPosition="bottom-right" initialIsOpen={false} />
      </Suspense>
    </QueryClientProvider>
  );
};

const init = async () => {
  if (!rootContainer) throw new Error("Failed to find the root element");

  const root = createRoot(rootContainer);

  root.render(
    <StrictMode>
      <QueryProvider />
    </StrictMode>,
  );
};

init();
