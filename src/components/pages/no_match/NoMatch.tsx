import { Link, useLocation } from "react-router-dom";

const NoMatch = (): JSX.Element => {
  const { pathname } = useLocation();

  return (
    <div className="bg-surface flex h-full w-full p-5">
      <div className="m-auto flex flex-col">
        <div className="mx-auto flex flex-col">
          <strong className="mx-auto">
            <pre className="my-0 text-orange-500" style={{ fontSize: "6em" }}>
              404
            </pre>
          </strong>
          <pre className="mx-auto my-0 w-[220px] truncate break-all">{pathname}</pre>
          <strong className="mx-auto">
            <pre className="my-0" style={{ fontSize: "2em" }}>
              <span>{"Not found"}</span>
            </pre>
          </strong>
        </div>

        <Link className="flex h-full w-full" to="/">
          <span className="m-auto">
            <span>{"Home"}</span>
          </span>
        </Link>
      </div>
    </div>
  );
};

export default NoMatch;
