import DashboardContainer from "components/organisms/dashboard_container/DashboardContainer";

const Dashboard = (): JSX.Element => {
  return (
    <div className="flex grow flex-col overflow-auto">
      <DashboardContainer />
    </div>
  );
};

export default Dashboard;
