import ImageCard from "../../atoms/image_card/ImageCard";
import { Link } from "react-router-dom";
import Spinner from "../../atoms/spinner/Spinner";
import { useFetchCats } from "./useFetchCats";

interface Props {
  className?: string;
}

const CatsView = ({ className }: Props): JSX.Element => {
  const { data, refetch, isFetching, isError, error } = useFetchCats(10);

  return (
    <div className={`flex flex-col items-center ${className}`}>
      <strong className="m-auto my-2 text-xl">
        {"Cats from: "}
        <Link to="https://api.thecatapi.com/v1" target="_blank">
          https://api.thecatapi.com/v1
        </Link>
      </strong>
      {isFetching ? (
        <Spinner />
      ) : isError ? (
        <div className="flex flex-col">
          <strong className="mb-2 text-xl">{error?.message}</strong>
          <button className="m-auto" onClick={() => refetch()}>
            Retry
          </button>
        </div>
      ) : (
        <>
          {data && (
            <>
              <div className="flex flex-wrap items-center justify-center">
                {data.map((item, i) => (
                  <ImageCard
                    key={`${item.id}-${i}`}
                    width={280}
                    height={280}
                    url={item.url}
                    alt={`Cat ${i}`}
                  />
                ))}
              </div>
            </>
          )}
        </>
      )}
    </div>
  );
};

export default CatsView;
