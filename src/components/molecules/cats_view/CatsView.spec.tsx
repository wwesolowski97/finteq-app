import * as useFetchCatsModule from "./useFetchCats";

import { fireEvent, render, waitFor } from "@testing-library/react";

import { BrowserRouter } from "react-router-dom"; // Import BrowserRouter
import CatsView from "./CatsView";

// Sample cats data for testing
const sampleCats = [
  { id: "1", url: "https://cat1.com", width: 500, height: 500 },
  { id: "2", url: "https://cat2.com", width: 500, height: 500 },
];

jest.mock("./useFetchCats", () => ({
  __esModule: true,
  useFetchCats: jest.fn(),
}));

describe("CatsView component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should render loading spinner initially", async () => {
    (useFetchCatsModule.useFetchCats as jest.Mock).mockReturnValue({
      data: undefined,
      refetch: jest.fn(),
      isFetching: true,
      isError: false,
      error: null,
    });

    const { getByTestId } = render(
      <BrowserRouter>
        <CatsView />
      </BrowserRouter>,
    );

    expect(getByTestId("spinner")).toBeTruthy();
  });

  it("should render error message and retry button if fetch fails", async () => {
    (useFetchCatsModule.useFetchCats as jest.Mock).mockReturnValue({
      data: undefined,
      refetch: jest.fn(),
      isFetching: false,
      isError: true,
      error: { name: "error", message: "Failed to fetch cats" },
    });

    const { getByText } = render(
      <BrowserRouter>
        <CatsView />
      </BrowserRouter>,
    );

    fireEvent.click(getByText("Retry"));
    expect(useFetchCatsModule.useFetchCats).toHaveBeenCalledTimes(1);
  });

  it("should render cat images when fetch is successful", async () => {
    (useFetchCatsModule.useFetchCats as jest.Mock).mockReturnValue({
      data: sampleCats,
      refetch: jest.fn(),
      isFetching: false,
      isError: false,
      error: null,
    });

    const { getByAltText } = render(
      <BrowserRouter>
        <CatsView />
      </BrowserRouter>,
    );

    await waitFor(() => {
      expect(getByAltText("Cat 0")).toBeTruthy();
      expect(getByAltText("Cat 1")).toBeTruthy();
    });
  });
});
