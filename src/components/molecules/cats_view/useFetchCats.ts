import { getCats } from "../../../api/cats/get";
import { useQuery } from "@tanstack/react-query";

export const useFetchCats = (limit: number) => {
  const { data, refetch, isFetching, isError, error } = useQuery({
    queryKey: ["cats_key", limit],
    queryFn: () => getCats(limit),
  });

  return { data, refetch, isFetching, isError, error };
};
