import { getDogs } from "api/dogs/get";
import { useQuery } from "@tanstack/react-query";

export const useFetchDogs = (limit: number) => {
  const { data, refetch, isFetching, isError, error } = useQuery({
    queryKey: ["dogs_key", limit],
    queryFn: () => getDogs(limit),
  });

  return { data, refetch, isFetching, isError, error };
};
