import * as useFetchDogsModule from "./useFetchDogs";

import { fireEvent, render, waitFor } from "@testing-library/react";

import { BrowserRouter } from "react-router-dom"; // Import BrowserRouter
import DogsView from "./DogsView";

// Sample cats data for testing
const sampleDogs = {
  message: ["https://dog1.com", "https://dog2.com"],
  status: 200,
};

jest.mock("./useFetchDogs", () => ({
  __esModule: true,
  useFetchDogs: jest.fn(),
}));

describe("CatsView component", () => {
  beforeEach(() => {
    jest.clearAllMocks();
  });

  it("should render loading spinner initially", async () => {
    (useFetchDogsModule.useFetchDogs as jest.Mock).mockReturnValue({
      data: undefined,
      refetch: jest.fn(),
      isFetching: true,
      isError: false,
      error: null,
    });

    const { getByTestId } = render(
      <BrowserRouter>
        <DogsView />
      </BrowserRouter>,
    );

    expect(getByTestId("spinner")).toBeTruthy();
  });

  it("should render error message and retry button if fetch fails", async () => {
    (useFetchDogsModule.useFetchDogs as jest.Mock).mockReturnValue({
      data: undefined,
      refetch: jest.fn(),
      isFetching: false,
      isError: true,
      error: { name: "error", message: "Failed to fetch cats" },
    });

    const { getByText } = render(
      <BrowserRouter>
        <DogsView />
      </BrowserRouter>,
    );

    fireEvent.click(getByText("Retry"));
    expect(useFetchDogsModule.useFetchDogs).toHaveBeenCalledTimes(1);
  });

  it("should render cat images when fetch is successful", async () => {
    (useFetchDogsModule.useFetchDogs as jest.Mock).mockReturnValue({
      data: sampleDogs,
      refetch: jest.fn(),
      isFetching: false,
      isError: false,
      error: null,
    });

    const { getByAltText } = render(
      <BrowserRouter>
        <DogsView />
      </BrowserRouter>,
    );

    await waitFor(() => {
      expect(getByAltText("Dog 0")).toBeTruthy();
      expect(getByAltText("Dog 1")).toBeTruthy();
    });
  });
});
