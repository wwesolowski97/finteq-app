import ImageCard from "../../atoms/image_card/ImageCard";
import { Link } from "react-router-dom";
import Spinner from "../../atoms/spinner/Spinner";
import { useFetchDogs } from "./useFetchDogs";

interface Props {
  className?: string;
}

const DogsView = ({ className }: Props): JSX.Element => {
  const { data, refetch, isFetching, isError, error } = useFetchDogs(10);

  return (
    <div className={`flex flex-col items-center ${className}`}>
      <strong className="m-auto my-2 text-xl">
        {"Dogs from: "}
        <Link to="https://dog.ceo/api" target="_blank">
          https://dog.ceo/api
        </Link>
      </strong>
      {isFetching ? (
        <Spinner />
      ) : isError ? (
        <div className="flex flex-col">
          <strong className="mb-2 text-xl">{error?.message}</strong>
          <button className="m-auto" onClick={() => refetch()}>
            Retry
          </button>
        </div>
      ) : (
        <>
          {data && (
            <>
              <div className="flex flex-wrap items-center justify-center">
                {data.message.map((item, i) => (
                  <ImageCard
                    key={`${item}-${i}`}
                    width={280}
                    height={280}
                    url={item}
                    alt={`Dog ${i}`}
                  />
                ))}
              </div>
            </>
          )}
        </>
      )}
    </div>
  );
};

export default DogsView;
