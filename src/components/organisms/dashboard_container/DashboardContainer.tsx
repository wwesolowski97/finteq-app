import CatsView from "components/molecules/cats_view/CatsView";
import DogsView from "components/molecules/dogs_view/DogsView";

const DashboardContainer = (): JSX.Element => {
  return (
    <>
      <CatsView className="mb-6" />
      <DogsView />
    </>
  );
};

export default DashboardContainer;
