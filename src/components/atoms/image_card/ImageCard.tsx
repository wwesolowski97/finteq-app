interface Props {
  width?: number;
  height?: number;
  url: string;
  alt: string;
}

const ImageCard = ({ width = 200, height = 200, url, alt }: Props): JSX.Element => {
  return (
    <div className="p-2" style={{ width, height }}>
      <img className="h-full w-full object-cover" src={url} alt={alt} />
    </div>
  );
};

export default ImageCard;
