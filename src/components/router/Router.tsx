import { Navigate, Route, Routes } from "react-router-dom";
import { Suspense, lazy } from "react";

import Spinner from "components/atoms/spinner/Spinner";

const Dashboard = lazy(() => import("components/pages/dashboard/Dashboard"));
const NoMatch = lazy(() => import("components/pages/no_match/NoMatch"));

const Router = (): JSX.Element => {
  return (
    <>
      <Routes>
        <Route path="/" element={<Navigate to="dashboard" replace />} />
        <Route
          path="dashboard"
          element={
            <Suspense fallback={<Spinner />}>
              <Dashboard />
            </Suspense>
          }
        />

        <Route
          path="*"
          element={
            <Suspense fallback={<Spinner />}>
              <NoMatch />
            </Suspense>
          }
        />
      </Routes>
    </>
  );
};

export default Router;
