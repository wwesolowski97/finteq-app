import { getApi } from "../common";

const BASE_URL = "https://api.thecatapi.com/v1";

interface CatResponse {
  id: string;
  url: string;
  width: number;
  height: number;
}

export const getCats = (limit: number) =>
  getApi()
    .get<CatResponse[]>(`${BASE_URL}/images/search?limit=${limit}`)
    .then(r => r.data);
