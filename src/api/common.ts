import axios, { type AxiosInstance } from "axios";

export const getApi = (): AxiosInstance =>
  axios.create({
    headers: {
      "Content-type": "application/json",
    },
  });
