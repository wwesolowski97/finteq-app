import { getApi } from "../common";

const BASE_URL = "https://dog.ceo/api";

interface DogsResponse {
  message: string[];
  status: string;
}

export const getDogs = (limit: number) => {
  return getApi()
    .get<DogsResponse>(`${BASE_URL}/breeds/image/random/${limit}`)
    .then(r => r.data);
};
