import "./App.css";

import { BrowserRouter } from "react-router-dom";
import Router from "components/router/Router";
import { Toaster } from "react-hot-toast";

const Main = (): JSX.Element => {
  return (
    <BrowserRouter>
      <Router />
      <Toaster position="bottom-right" />
    </BrowserRouter>
  );
};

export default Main;
