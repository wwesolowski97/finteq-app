# Finteq app

## Description
Fanteq app for fetching multiple services with React Query.

## Prerequisites
Before you begin, ensure you have met the following requirements:
- Node.js installed on your local machine.

## Installation
To install the required packages, follow these steps:
1. Clone the repository to your local machine:
   ```bash
   git clone https://gitlab.com/wwesolowski97/finteq-app.git
2. Navigate to the project directory:
   ```bash
   cd finteq-app
3. Install dependencies using npm:
   ```bash
   npm install
   
## Usage
To start the app, run the following command:

```bash
npm start
```
This will start the development server, and the app will be accessible at http://localhost:3000.
## Building the Project
To build the project for production, run the following command:
```bash
npm run build
```
This will create a production-ready build of your project in the build directory.
## Deployment
To deploy the project, follow these steps:

1. Ensure you have set up your deployment environment.
2. Run the build command to generate the production build.
3. Deploy the contents of the build directory to your deployment environment.

## Running Tests
To run tests, execute the following command:
```bash
npm test
```
This will run all the tests available in the project.

## License
```bash
No license for now :) Project for Finteq by Wojciech Wesołowski.